<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/' , 'WelcomController')->name('welcome');

Route::get('/resume' , 'ResumeController')->name('resume');

Route::get('/contact' , 'ContactController')->name('contact');
Route::post('/contact' , 'ContactController@dosend')->name('send');

Route::get('/reqproject' , 'ProjeController@pro')->name('needpro');
Route::post('/reqproject' , 'ProjeController@dosend')->name('sabt');