<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use App\View\Components\WorkExComponent;
use App\View\Components\EduComponent;
use App\View\Components\NavComponent;
use App\View\Components\InterestComponent;
use App\View\Components\MySiteComponent;
use App\View\Components\NeedProComponent;
use App\View\Components\ContactComponent;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        blade::component('work-ex',WorkExComponent::class);
        blade::component('edu-c',EduComponent::class);
        blade::component('nav-x',NavComponent::class);
        blade::component('interest-x',InterestComponent::class);
        blade::component('mysite-x',MySiteComponent::class);
        blade::component('needpro-x' , NeedProComponent::class);
        blade::component('contact-x' , ContactComponent::class);
        blade::if('showmap' , function() {
            return false ;
        } );


    }
}
