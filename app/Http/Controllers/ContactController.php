<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Site ;
class ContactController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        return view('contact');
    }
    public function dosend (Request $request){

            
            $site = new Site();
            $site->fullname = $request->input('fullname');
            $site->email = $request->input('email');
            $site->message =$request->input('message');
            $site->save();
            return redirect()->route('contact');

    }
}
