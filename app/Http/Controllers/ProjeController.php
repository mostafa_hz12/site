<?php

namespace App\Http\Controllers;
use App\Project ;
use Illuminate\Http\Request;

class ProjeController extends Controller
{
    public function pro(){

        return view ('needproject');
    }
    public function dosend(Request $request){

        //print_r($request->all()) ; die();

        $pr = new Project();
        $pr->fullname = $request->input('fullname');
        $pr->email = $request->input('email');
        $pr->subject =$request->input('subject');
        $pr->message =$request->input('message');
        $pr->save();
        return redirect()->route('project');
    }
}
