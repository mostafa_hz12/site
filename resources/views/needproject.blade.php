@extends('layouts.app')
@section('content')

<section class="col s12 m12 l8 section">
                    <div class="row">
                        <!-- Start map -->
                        @showmap
                        <div class="section-wrapper g-map z-depth-1">
                            <div id="map"> </div>
                        </div> 
                        @endshowmap
                        <!--=======================================
                         Contact
                        ==========================================-->

                        <x-needpro-x />
                        <!-- =========================================
                          portfolio Website
                        ==========================================-->

                        <x-mysite-x />
                    </div> 
                <!-- end row -->
                </section>



@endsection