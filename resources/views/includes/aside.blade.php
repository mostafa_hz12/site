<aside class="col l4 m12 s12 sidebar z-depth-1" id="sidebar">
                    <!--  Sidebar row -->
                    <div class="row">                      
                        <!--  top section   -->
                        <div class="heading">                            
                            <!-- ====================
                                      IMAGE   
                            ==========================-->
                            <div class="feature-img">
                                <a href="index.html"><img src="assets/images/profile-img.jpg" class="responsive-img" alt=""></a> 
                            </div>                            
                            <!-- =========================================
                                       NAVIGATION   
                            ==========================================-->
                          @include('includes.nav')                           
                            <!-- ========================================
                                       NAME AND TAGLINE
                            ==========================================-->
                            <div class="title col s12 m12 l9 right  wow fadeIn" data-wow-delay="0.1s">   
                                <h2>{{ __('content.aside.name') }}</h2> <!-- title name -->
                                <span>{{ __('content.aside.job') }}</span>  <!-- tagline -->
                            </div>                         
                        </div>
                         <!-- sidebar info -->
                        <div class="col l12 m12 s12 sort-info sidebar-item">
                            <div class="row">                               
                                <div class="col m12 s12 l3 icon"> <!-- icon -->
                                   <i class="fa fa-user"></i>
                                </div>                                
                                <div class="col m12 s12 l9 info wow fadeIn a1" data-wow-delay="0.1s" > <!-- text -->
                                    <div class="section-item-details">
                                        <p>{{ __('content.aside.explain') }}</p>
                                    </div>             
                                </div>
                            </div>         
                        </div>
                        <!-- MOBILE NUMBER -->
                        <div class="col l12 m12 s12  mobile sidebar-item">
                            <div class="row">                                
                                <div class="col m12 s12 l3 icon">
                                    <i class="fa fa-phone"></i> <!-- icon -->
                                </div>                                
                                <div class="col m12 s12 l9 info wow fadeIn a2" data-wow-delay="0.2s" >
                                    <div class="section-item-details">
                                        <div class="personal">
                                            <h4><a href="tel:555-555-5555">(111)-333-4444</a></h4> <!-- Number -->             
                                            <span>mobile</span> 
                                        </div>
                                        <div class="work">
                                            <h4><a href="tel:555-555-5555">(121)-323-3494</a></h4> <!-- Number -->
                                            <span>work</span> 
                                        </div>
                                    </div>
                                </div>
                            </div>             
                        </div>
                        <!--  EMAIL -->
                        <div class="col l12 m12 s12  email sidebar-item ">
                            <div class="row">                                
                                <div class="col m12 s12 l3 icon">
                                    <i class="fa fa-envelope"></i> <!-- icon -->
                                </div>                                
                                <div class="col m12 s12 l9 info wow fadeIn a3" data-wow-delay="0.3s">
                                    <div class="section-item-details">
                                        <div class="personal">                                    
                                            <h4><a href="mailto:someone@example.com">mail@alrayhan.com</a></h4> <!-- Email -->
                                            <span>personal</span> 
                                        </div>
                                        <div class="work">                                 
                                            <h4><a href="mailto:someone@example.com">ar@deviserweb.com</a></h4> <!-- Email -->
                                            <span>work</span> <br>
                                        </div>
                                    </div>
                                </div> 
                            </div>          
                        </div>
                        <!-- ADDRESS  -->
                        <div class="col l12 m12 s12  address sidebar-item ">
                            <div class="row">                                
                                <div class="col l3 m12  s12 icon">
                                    <i class="fa fa-home"></i> <!-- icon -->
                                </div>                                
                                <div class="col m12 s12 l9 info wow fadeIn a4" data-wow-delay="0.4s">
                                    <div class="section-item-details">
                                        <div class="address-details"> <!-- address  -->
                                            <h4>24 Golden Tower <span>(2nd floor)</span><br>
                                            Amborkhana, Sylhet.<br>
                                            <span>SYL-3RF87W</span></h4> 
                                        </div>                         
                                    </div>
                                </div>
                            </div>            
                        </div>
                        <!-- SKILLS -->
                        <div class="col l12 m12 s12  skills sidebar-item" >
                            <div class="row">
                                <div class="col m12 l3 s12 icon">
                                    <i class="fa fa-calendar-o"></i> <!-- icon -->
                                </div>
                                 <!-- Skills -->
                                <div class="col m12 l9 s12 skill-line a5 wow fadeIn" data-wow-delay="0.5s">
                                    <h3>Professional Skills </h3>
                                    
                                    <span>Adobe Photoshop</span>                                    
                                    <div class="progress">
                                        <div class="determinate"> 70% </div>
                                    </div>
                                    
                                    <span>HTML</span>
                                    <div class="progress">
                                        <div class="determinate"> 95%</div>
                                    </div>
                                    
                                    <span>CSS</span>
                                    <div class="progress">
                                        <div class="determinate">90%</div>
                                    </div>

                                    <span>Javascript</span>
                                    <div class="progress">
                                        <div class="determinate">85%</div>
                                    </div>

                                    <span>PHP</span>
                                    <div class="progress">
                                        <div class="determinate">70%</div>
                                    </div>

                                    <span>JAVA</span>
                                    <div class="progress">
                                        <div class="determinate">55%</div>
                                    </div>

                                    <span>SQL</span>
                                    <div class="progress">
                                        <div class="determinate">40%</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>   <!-- end row -->
                </aside><!-- end sidebar -->
