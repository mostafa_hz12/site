<div class="section-wrapper z-depth-1">                           
                            <div class="section-icon col s12 m12 l2">
                                <i class="fa fa-plane"></i>
                            </div>
                            <div class="interests col s12 m12 l10 wow fadeIn" data-wow-delay="0.1s"> 
                                <h2>Interestes </h2>
                                <ul> <!-- interetsr icon start -->
                                    <li><i class="fa fa-camera-retro tooltipped" data-position="top" data-delay="50" data-tooltip="Photography"></i></li>
                                    <li><i class="fa fa-glass tooltipped" data-position="top" data-delay="50" data-tooltip="Drinking"></i></li>
                                    <li><i class="fa fa-headphones tooltipped" data-position="top" data-delay="50" data-tooltip="Music"></i></li>
                                    <li><i class="fa fa-comments tooltipped" data-position="top" data-delay="50" data-tooltip="Chatting"></i></li>
                                    <li><i class="fa fa-coffee tooltipped" data-position="top" data-delay="50" data-tooltip="Coffee"></i></li>
                                </ul> <!-- interetsr icon end -->
                            </div>                          
                        </div>