<div class="section-wrapper z-depth-1">                            
                            <div class="section-icon col s12 m12 l2">
                                <i class="fa fa-paper-plane-o"></i>
                            </div>
                            <div class="col s12 m12 l10 wow fadeIn a1" data-wow-delay="0.1s">
                                
                                <h2>{{ __('content.contact.name') }}</h2>                                
                                <div class="contact-form" id="contact">   
                                    <div class="row">                                   
                                        <form role="form" id="contactForm" data-toggle="validator" method="POST" action="{{ route('send') }}">
                                        @csrf
                                            <div id="msgSubmit" class="h3 text-center hidden"></div>
                                            <div class="input-field col s12">
                                                <label for="name" class="h4">{{ __('content.contact.fullname') }} *</label>
                                                <input type="text" class="form-control validate" id="name" name="fullname" required data-error="NEW ERROR MESSAGE">
                                            </div>
                                            <div class="input-field col s12">
                                                <label for="email" class="h4">{{ __('content.contact.email') }} *</label>
                                                <input type="email" class="form-control validate" id="email" name="email" required>                
                                            </div>
                                            <div class="input-field col s12">
                                                <label for="message" class="h4 ">{{ __('content.contact.message') }} *</label>
                                                <textarea id="message" class="form-control materialize-textarea validate" name="message" required></textarea>           
                                            </div>
                                            <button type="submit" id="form-submit" class="btn btn-success">ارسال</button>
                                            <div class="clearfix"></div>                                
                                        </form>                                     
                                    </div> 
                                </div>
                            </div>                            
                         </div>