<div class="section-wrapper z-depth-1">                            
                            <div class="section-icon col s12 m12 l2">
                                <i class="fa fa-suitcase"></i>
                            </div>
                            <div class="custom-content col s12 m12 l10 wow fadeIn a1" data-wow-delay="0.1s">
                                <h2>Work Experience</h2>

                                <div class="custom-content-wrapper wow fadeIn a2" data-wow-delay="0.2s">
                                    <h3>UI/UX Designer <span>@Academy</span></h3>
                                    <span>JAN 2013 - DEC 2013 </span>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                                        labore et dolore magna aliqua. </p>
                                </div>
                                <div class="custom-content-wrapper wow fadeIn a3" data-wow-delay="0.3s">
                                    <h3>Creative Director <span>@DeviserWeb</span></h3>
                                    <span>JAN 2013 - DEC 2013 </span>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et 
                                        dolore magna aliqua. </p>
                                </div>
                                <div class="custom-content-wrapper wow fadeIn a4" data-wow-delay="0.4s">
                                    <h3>Graphics Designer <span>@Creative Wrold</span></h3>
                                    <span>JAN 2013 - DEC 2013 </span>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
                                        labore et dolore magna aliqua. </p>
                                </div>
                            </div>                            
                        </div>
