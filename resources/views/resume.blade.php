@extends('layouts.app')
@section('content')
<section class="col s12 m12 l8 section">
                    <div class="row">
                       <x-work-ex />
                        <!-- ========================================
                         Education 
                        ==========================================-->
                        <x-edu-c />

                        <!-- ========================================
                              Intertests 
                        ==========================================-->

                        <x-interest-x />
                        <!-- =======================================
                          portfolio Website
                        ==========================================-->

                        <x-mysite-x />
                    </div><!-- end row -->
                </section><!-- end section -->
@endsection