@extends('layouts.app')
@section('content')
<section class="col s12 m12 l8 section">
                    <div class="row">
                        <!-- Start map -->
                        
                        <div class="section-wrapper g-map z-depth-1">
                            <div id="map"> </div>
                        </div> 
                        
                        <!--=======================================
                         Contact
                        ==========================================-->

                        <x-contact-x />
                        <!-- =========================================
                          portfolio Website
                        ==========================================-->

                        <x-mysite-x />
                    </div> 
                <!-- end row -->
                </section>
@endsection

@push('scripts')
 <!-- Map api -->
 <script src="http://maps.googleapis.com/maps/api/js?v=3.exp"></script>
@endpush